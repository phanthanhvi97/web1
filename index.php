<?php
include_once("GUI/lib/dataprovider.php");
include_once("DAO/DB.php");
include_once ("DAO/LoaiSanPhamDAO.php");
include_once ("BUS/LoaiSanPhamBUS.php");
include_once ("DTO/LoaiSanPham.php");

include_once ("DAO/HangSanXuatDAO.php");
include_once ("BUS/HangSanXuatBUS.php");
include_once ("DTO/HangSanXuat.php");

include_once ("DAO/SanPhamDAO.php");
include_once ("BUS/SanPhamBUS.php");
include_once ("DTO/SanPham.php");

include_once ("DAO/TaiKhoanDAO.php");
include_once ("BUS/TaiKhoanBUS.php");
include_once ("DTO/TaiKhoan.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>kid shop</title>
    <link rel="stylesheet" type="text/css" href="GUI/css/style.css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <?php include("GUI/modules/mheader.php"); ?>
    </div>
    <div id="nav_login">
        <?php include("GUI/modules/mLogin/mlogin.php");?>
    </div>
    <div id="search">
        <?php include("GUI/modules/msearch/msearch.php");?>
    </div>
    <div id="nav">
        <div id="menuup">
            <?php include("GUI/modules/mmenu/mmenuup.php"); ?>
        </div>
        <div id="menudow">
            <?php include("GUI/modules/mmenu/mmenudow.php"); ?>
        </div>

    </div>
    <div id="content">

        <?php include("GUI/modules/mcontent.php"); ?>
    </div>

    <div id="footer"><?php include("GUI/modules/mfooter.php"); ?></div>
</div>
</body>
</html>