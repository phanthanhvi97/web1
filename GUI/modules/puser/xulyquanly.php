<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 11:30 PM
 */

include_once ("../../../DAO/DB.php");
include_once ("../../../DAO/DonDatHangDAO.php");
include_once ("../../../BUS/DonDatHangBUS.php");
include_once ("../../../DTO/DonDatHang.php");

include_once ("../../../DAO/ChiTietDonHangDAO.php");
include_once ("../../../BUS/ChiTietDonHangBUS.php");
include_once ("../../../DTO/ChiTietDonHang.php");

include_once ("../../../DAO/SanPhamDAO.php");
include_once ("../../../BUS/SanPhamBUS.php");
include_once ("../../../DTO/SanPham.php");

// bên nút quản lý tài khoản chọn nút thêm sản phẩm
if($_GET["a"] == 1)
{
    $chitietBUS = new ChiTietDonHangBUS();
    $chitietBUS->UpdateThem($_GET["id"]);
}
//bên nút quản lý tài khoản chọn nút giảm sản phẩm
else if ($_GET["a"] == 2)
{
    $chitietBUS = new ChiTietDonHangBUS();
    $chitietBUS->UpadteGiam($_GET["id"]);
}
//bên nút quản lý tài khoản chọn nút hủy sản phẩm
else if ($_GET["a"] == 3)
{

    $donhangBUS = new DonDatHangBUS();
    $donhangBUS->UpdateHUY($_GET["id"]);
}
// bên quản lý tài khoản chọn nút thanh toán
else if ($_GET["a"] == 4)
{
    $sanphamBUS = new SanPhamBUS();
    $sanphamBUS->UpdateGia($_GET["id"], $_GET["sl"]);
}
//trở về trang đầu sau khi sử lý xong
?>
<script>
    window.history.back();
</script>
