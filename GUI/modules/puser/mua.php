<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 8:02 PM
 */
include_once ("../../../DAO/DB.php");
include_once ("../../../DAO/DonDatHangDAO.php");
include_once ("../../../BUS/DonDatHangBUS.php");
include_once ("../../../DTO/DonDatHang.php");

include_once ("../../../DAO/ChiTietDonHangDAO.php");
include_once ("../../../BUS/ChiTietDonHangBUS.php");
include_once ("../../../DTO/ChiTietDonHang.php");

include_once ("../../../DAO/SanPhamDAO.php");
include_once ("../../../BUS/SanPhamBUS.php");
include_once ("../../../DTO/SanPham.php");

//nhận tất cả thông tin từ bên kia gửi qua bào gồm mã sản phẩm
//tiền , số lượng, mã tại khoản
$tk = $_GET["tk"];
$sp = $_GET["sp"];
$tien= $_GET["tien"];
$sl = $_GET["sl"];

$donHang = new DonDatHangBUS();
//insert đơn hàng
$donHang->InsertTheoTaiKhoan($tk, $tien);

//insert chi tiết vào
//vì ta insert nguyên chi tiể có quá tring tạo chi tiết trước
$chiTietBUS = new ChiTietDonHangBUS();
$chiTiet = new ChiTietDonHang();
$chiTiet->MaDonDatHang =$donHang->Count() ;
$chiTiet->MaSanPham = $sp;
$mahang = ($chiTietBUS->Count() + 1);//mã đơn hàng thêm sau cùng + 1 là mã chi tiết
$chiTiet->MaChiTietDonDatHang = $mahang;
$sanPhamBUS = new SanPhamBUS();
$sanPham = $sanPhamBUS->GetByID($sp);
$chiTiet->GiaBan = $sanPham->GiaSanPham;
$chiTiet->SoLuong= $sl;

//insert vào rồi đá vào trang quản lý
$chiTietBUS->Insert($chiTiet);
header ("location:../../../index.php?a=7");

?>