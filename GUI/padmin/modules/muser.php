<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 5:43 AM
 */
include_once ("../../../DAO/DB.php");
include_once ("../../../BUS/TaiKhoanBUS.php");
include_once ("../../../DAO/TaiKhoanDAO.php");
include_once ("../../../DTO/TaiKhoan.php");



if(isset($_GET["them"]))
{
    $taiKhoanBUS = new TaiKhoanBUS();
    $taikhoan = new TaiKhoan();

    $mataikhoan = $_GET["mataikhoan"];
    $tendangnhap = $_GET["tendangnhap"];
    $matkhau = $_GET["matkhau"];
    $tenhienthi = $_GET["tenhienthi"];
    $diachi = $_GET["diachi"];
    $email = $_GET["email"];
    $bixoa = $_GET["bixoa"];
    $maloaitaikhoan = $_GET["maloaitaikhoan"];
    $ngaysinh = $_GET["ngaysinh"];


    $taikhoan->MaTaiKhoan = $mataikhoan;
    $taikhoan->TenDangNhap= $tendangnhap;
    $taikhoan->MatKhau= $matkhau;
    $taikhoan->TenHienThi= $tenhienthi;
    $taikhoan->DiaChi =$diachi ;
    $taikhoan->Email= $email;
    $taikhoan->BiXoa= $bixoa;
    $taikhoan->MaLoaiTaiKhoan= $maloaitaikhoan;
    $taikhoan->NgaySinh=$ngaysinh;

    $taiKhoanBUS->Insert($taikhoan);
    header("location:../../../admin.php");

}
else if(isset($_GET["sua"])){
    $taiKhoanBUS = new TaiKhoanBUS();
    $mataikhoan = (int)$_GET["id"];
////    echo"tai khoan $mataikhoan";
////    if (is_numeric($mataikhoan))
////        echo 'Biến này là số';
//    else
//        echo 'Biến k này là số';


    if($_GET["tendangnhap"] !="")
    {
//        $a = $_GET["tendangnhap"];
//        echo"tendangnhap $a";
        $taiKhoanBUS->UpdateTenDN($mataikhoan, $_GET["tendangnhap"]);
    }
    if($_GET["matkhau"] !="")
    {
//        $a = $_GET["matkhau"];
//        echo"matkhau $a";
        $taiKhoanBUS->UpdateMK($mataikhoan,$_GET["matkhau"]);
    }
    if($_GET["tenhienthi"] !="")
    {
//        $a = $_GET["tendangnhap"];
//        echo"tendangnhap $a";
        $taiKhoanBUS->UpdateTenHienThi($mataikhoan, $_GET["tenhienthi"]);
    }

    header("location:../../../admin.php");


}
?>
