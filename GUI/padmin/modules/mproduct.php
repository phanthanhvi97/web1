<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 5:42 AM
 */
include_once ("../../../DAO/DB.php");
include_once ("../../../BUS/SanPhamBUS.php");
include_once ("../../../DAO/SanPhamDAO.php");
include_once ("../../../DTO/SanPham.php");



if(isset($_GET["them"]))
{
    $sanphamBUS = new SanPhamBUS();
   $sanpham = new SanPham();

    $masanpham = $_GET["masanpham"];
    $tensanham = $_GET["tensanham"];
    $hinhurl = $_GET["hinhurl"];
    $giasanpham = $_GET["giasanpham"];
    $ngaynhap = $_GET["ngaynhap"];
    $soluongton = $_GET["soluongton"];
    $soluongban = $_GET["soluongban"];
    $soluotxem = $_GET["soluotxem"];
    $mota = $_GET["mota"];
    $matinhtrang = $_GET["matinhtrang"];
    $maloaisanpham = $_GET["maloaisanpham"];
    $mahangsanxuat = $_GET["mahangsanxuat"];



    $sanpham->MaSanPham = $masanpham;
    $sanpham->TenSanPham= $tensanham;
    $sanpham->HinhURL= $hinhurl;
    $sanpham->GiaSanPham= $giasanpham;
    $sanpham->NgayNhap =$ngaynhap ;
    $sanpham->SoLuongTon= $soluongton;
    $sanpham->SoLuongBan= $soluongban;
    $sanpham->SoLuocXem= $soluotxem;
    $sanpham->MoTa=$mota;
    $sanpham->BiXoa =$matinhtrang ;
    $sanpham->MaLoaiSanPham= $maloaisanpham;
    $sanpham->MaHangSanXuat= $mahangsanxuat;

    $sanphamBUS->Insert($sanpham);
    header("location:../../../admin.php");

}
else if(isset($_GET["sua"])){
    $sanphamBUS = new SanPhamBUS();
    $sanpham = (int)$_GET["id"];

    if($_GET["tensanham"] !="")
    {
        $sanphamBUS->UpdateTen($sanpham, $_GET["tensanham"]);
    }
    header("location:../../../admin.php");

}
?>

