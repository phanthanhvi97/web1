<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 5:43 AM
 */

include_once ("../../../DAO/DB.php");
include_once ("../../../BUS/LoaiSanPhamBUS.php");
include_once ("../../../DAO/LoaiSanPhamDAO.php");
include_once ("../../../DTO/LoaiSanPham.php");

if(isset($_GET["them"]))
{

    $loaiSanPhamBUS = new LoaiSanPhamBUS();
    $loai = new LoaiSanPham();

    $maloaisanpham = $_GET["maloaisanpham"];
    $tenloaisanpham = $_GET["tenloaisanpham"];
    $bixoa = $_GET["bixoa"];

    $loai->MaLoaiSanPham = $maloaisanpham;
    $loai->TenLoaiSanPham= $tenloaisanpham;
    $loai->BiXoa= $bixoa;
    $loaiSanPhamBUS->Insert($loai);
    header("location:../../../admin.php");

}
else if(isset($_GET["sua"])){
    $loaiSanPhamBUS = new LoaiSanPhamBUS();
    $ma = (int)$_GET["id"];
    if($_GET["tenloaisanpham"] !="")
    {
        $loaiSanPhamBUS->UpdateTen($ma, $_GET["tenloaisanpham"]);
    }

    header("location:../../../admin.php");
}
?>

