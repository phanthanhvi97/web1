<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 5:42 AM
 */
include_once ("../../../DAO/DB.php");
include_once ("../../../BUS/HangSanXuatBUS.php");
include_once ("../../../DAO/HangSanXuatDAO.php");
include_once ("../../../DTO/HangSanXuat.php");



if(isset($_GET["them"]))
{
$hangSanXuatBUS = new HangSanXuatBUS();
$hang = new HangSanXuat();

$mahangsanxuat = $_GET["mahangsanxuat"];
$tenhangsanxuat = $_GET["tenhangsanxuat"];
$logourl = $_GET["logourl"];
$bixoa = $_GET["bixoa"];

    $hang->MaHangSanXuat = $mahangsanxuat;
    $hang->TenHangSanXuat= $tenhangsanxuat;
    $hang->LogoURL= $logourl;
    $hang->BiXoa= $bixoa;

    $hangSanXuatBUS->Insert($hang);
header("location:../../../admin.php");

}
else if(isset($_GET["sua"])){
    $hangSanXuatBUS = new HangSanXuatBUS();
$ma = (int)$_GET["id"];


if($_GET["tenhangsanxuat"] !="")
{
    $hangSanXuatBUS->UpdateTen($ma, $_GET["tenhangsanxuat"]);
}
if($_GET["logourl"] !="")
{
    $hangSanXuatBUS->UpdateLogo($ma,$_GET["logourl"]);
}

header("location:../../../admin.php");


}
?>

