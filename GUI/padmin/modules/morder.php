<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 5:42 AM
 */
include_once ("../../../DAO/DB.php");
include_once ("../../../BUS/DonDatHangBUS.php");
include_once ("../../../DAO/DonDatHangDAO.php");
include_once ("../../../DTO/DonDatHang.php");



if(isset($_GET["them"]))
{
    $donHangBUS = new DonDatHangBUS();
    $don = new DonDatHang();

    $madondathang = $_GET["madondathang"];
    $ngaylap = $_GET["ngaylap"];
    $tongthanhtien = $_GET["tongthanhtien"];
    $mataikhoan = $_GET["mataikhoan"];
    $matinhtrang = $_GET["matinhtrang"];


    $don->MaDonDatHang = $madondathang;
    $don->NgayLap= $ngaylap;
    $don->TongThanhTien= $tongthanhtien;
    $don->MaTaiKhoan= $mataikhoan;
    $don->MaTinhTrang =$matinhtrang ;


    $donHangBUS->Insert($don);
    header("location:../../../admin.php");

}
else if(isset($_GET["sua"])){
    $donHangBUS = new DonDatHangBUS();
    $ma =$_GET["id"];

    if($_GET["tongthanhtien"] !="")
    {
        $donHangBUS->UpdateTien($ma, $_GET["tongthanhtien"]);
    }
    header("location:../../../admin.php");

}
?>

