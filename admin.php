<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/16/2018
 * Time: 12:29 AM
 */
include_once("GUI/lib/dataprovider.php");
include_once("DAO/DB.php");
include_once ("DAO/LoaiSanPhamDAO.php");
include_once ("BUS/LoaiSanPhamBUS.php");
include_once ("DTO/LoaiSanPham.php");

include_once ("DAO/HangSanXuatDAO.php");
include_once ("BUS/HangSanXuatBUS.php");
include_once ("DTO/HangSanXuat.php");

include_once ("DAO/SanPhamDAO.php");
include_once ("BUS/SanPhamBUS.php");
include_once ("DTO/SanPham.php");

include_once ("DAO/TaiKhoanDAO.php");
include_once ("BUS/TaiKhoanBUS.php");
include_once ("DTO/TaiKhoan.php");

include_once ("DAO/DonDatHangDAO.php");
include_once ("BUS/DonDatHangBUS.php");
include_once ("DTO/DonDatHang.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>kid shop</title>
    <link rel="stylesheet" type="text/css" href="GUI/css/style.css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <?php include("GUI/padmin/modules/mheader.php"); ?>
    </div>
    <div id="nav">
        <?php include("GUI/padmin/modules/mmenu.php"); ?>
    </div>
    <div id="content">
        <?php include("GUI/padmin/modules/mcontent.php"); ?>
    </div>
</div>
</body>
</html>
