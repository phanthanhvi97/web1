<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/14/2018
 * Time: 9:49 PM
 */

class LoaiSanPhamBUS
{
    var $loaiSanPhamDAO;
    public function __construct ()
    {
        $this->loaiSanPhamDAO = new LoaiSanPhamDAO() ;
    }

    //;lấy tất cả
    public function GetAll()
    {
        return   $this->loaiSanPhamDAO ->GetAll();
    }
    //count
    public function Count()
    {
        return   $this->loaiSanPhamDAO ->Count();
    }

    //lấy cho người  dùng xem với xóa = 0
    public function GettAllAvailable()
    {
        return $this->loaiSanPhamDAO->GetAllAvailable();
    }

    //lấy sản phẩn theo một mã sản phẩm nào đó
    public function GetByID($maLoaiSanPham)
    {
        return $this ->loaiSanPhamDAO->GetByID($maLoaiSanPham);
    }

    //inset một sản phẩm
    public function Insert($loaiSanPham)
    {
        $this ->loaiSanPhamDAO ->Insert($loaiSanPham);
    }

    // insert bởi tên thôi
    public function  InsertWithName($tenLoaiSanpham)
    {
        $loaiSanPham = new LoaiSanPham();
        $loaiSanPham ->TenLoaiSanPham = $tenLoaiSanpham;
        $this ->loaiSanPhamDAO ->Insert($loaiSanPham);
    }

    //delete theo sản phẩm
    public function Delete($maSanPham)
    {
        $loaiSanPham = new LoaiSanPham();
        $loaiSanPham ->MaLoaiSanPham = $maSanPham;
        $soSanPham = $this ->loaiSanPhamDAO ->DemSoSanPhamThuocVeLoai($maSanPham);
        if ($soSanPham == 0)
            $this ->loaiSanPhamDAO ->Delete($loaiSanPham);
        else
            $this ->loaiSanPhamDAO->SetDelete($loaiSanPham);
    }

    //set delete theo loaisanpham
    public function SetDelete($maSanPham)
    {
        $this ->loaiSanPhamDAO ->SetDelete($maSanPham);
    }

    //unsetdelete theo masanpham
    public function UnSetDelete ($maSanPham )
    {
        $loaiSanPham = new LoaiSanPham();
        $loaiSanPham ->MaLoaiSanPham = $maSanPham;
        $this ->$this->loaiSanPhamDAO ->UnSetDelete($loaiSanPham);
    }

    //update theo loaisanpham

    public function Update ($loaiSanPham)
    {
        $this ->loaiSanPhamDAO ->Update($loaiSanPham);
    }

    //update theo tên
    public function UpdateWithName ($tenLoaiSanPham)
    {
        $loaiSanPham = new LoaiSanPham();
        $loaiSanPham ->TenLoaiSanPham= $tenLoaiSanPham;
        $this ->loaiSanPhamDAO->Update($loaiSanPham);
    }

    //update tên
    public function UpdateTen($ma, $ten)
    {
       $this->loaiSanPhamDAO->UpdateTen($ma, $ten);
    }
}