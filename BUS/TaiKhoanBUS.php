<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:57 AM
 */

class TaiKhoanBUS
{
    var $taiKhoanDAO ;

    public function __construct()
    {
        $this->taiKhoanDAO = new TaiKhoanDAO();
    }

    public function GetAll()
    {
        return $this->taiKhoanDAO->GetAll();
    }

    public function GetAllAvailable()
    {
        return $this->taiKhoanDAO->GetAllAvailable();
    }

    public function xllogin($us, $ps)
    {
        if( $this->taiKhoanDAO->xllogin($us, $ps) == null)
            return ;
        return $this->taiKhoanDAO->xllogin($us, $ps);
    }

    public function ktnamelogin($name)
    {
        return $this->taiKhoanDAO->ktnamelogin($name);
    }
    public function CreateAccountCode()
    {
        $kq =$this->taiKhoanDAO->CreateAccountCode();
        return $kq+1;
    }

    public function Insertuser($a, $b, $c, $d, $e, $f, $g, $h)
    {
        $taiKhoan = new TaiKhoan();
        $taiKhoan->MaTaiKhoan = $a;
        $taiKhoan->TenDangNhap = $b;
        $taiKhoan->MatKhau = $c;
        $taiKhoan->TenHienThi = $d;
        $taiKhoan->NgaySinh = $e;
        $taiKhoan->DiaChi = $f;
        $taiKhoan->DienThoai = $g;
        $taiKhoan->Email = $h;
        $taiKhoan->BiXoa = 0;
        $taiKhoan->MaLoaiTaiKhoan = 2;
        $this->taiKhoanDAO->Insert($taiKhoan);
    }

    public function Delete($taiKhoan)
    {
        $kt = $this->taiKhoanDAO->ktHDTaiKhoan($taiKhoan);
        if ($kt == 1)
        {
            return $this->taiKhoanDAO->SetDelete($taiKhoan);
        }
        else
        {
            return $this->taiKhoanDAO->Delete($taiKhoan);
        }

    }
    public function SetDelete($taiKhoan)
    {
        $this->taiKhoanDAO->SetDelete($taiKhoan);
    }

    public function Insert ($taiKhoan)
    {
        $this->taiKhoanDAO->Insert($taiKhoan);
    }

    public function UnsetDelete($taiKhoan)
    {
        $maTaiKhoan = new TaiKhoan();
        $maTaiKhoan->MaLoaiSanPham = $taiKhoan;
        $this->taiKhoanDAO->UnSetDelete($maTaiKhoan);
    }
    public function SetCategory($taiKhoan)
    {
        $this->taiKhoanDAO->SetCategory($taiKhoan);
    }

    public function Update($taiKhoan)
    {
        $maTaiKhoan = new TaiKhoan();
        $maTaiKhoan->MaLoaiSanPham = $taiKhoan;
        $this->taiKhoanDAO->Update($maTaiKhoan);
    }

    public function  Count(){
        return $this->taiKhoanDAO->Count();
    }

    public function GetID($id)
    {
        return $this->taiKhoanDAO->GetID($id);
    }

//update theo tên đăng nhập
    public function UpdateTenDN($id, $tendangnhap)
    {
       $this->taiKhoanDAO->UpdateTenDN($id, $tendangnhap);
    }

    //update theo matkhau
    public function UpdateMK($id, $matkhau)
    {
        $this->taiKhoanDAO->UpdateMK($id, $matkhau);
    }

    //update theo tên hiển thị
    public function UpdateTenHienThi($id, $tenhienthi)
    {$this->taiKhoanDAO->UpdateTenHienThi($id, $tenhienthi);
    }
}