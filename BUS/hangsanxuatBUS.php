<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:56 AM
 */

class HangSanXuatBUS
{
    var $hangSanXuatDAO;
    public function __construct()
    {
        $this->hangSanXuatDAO= new HangSanXuatDAO();
    }
    public function GetALL()
    {
        return $this->hangSanXuatDAO->GetAll();
    }
    public function Count()
    {
        return $this->hangSanXuatDAO->Count();
    }
    public function GetALLAvailable()
    {
        return $this->hangSanXuatDAO->GetAllAvailable();
    }
    public function GetByID($maHangSanXuat)
    {
        return $this->hangSanXuatDAO->GetByID($maHangSanXuat);
    }
    public function Insert($hangSanXuat)
    {
        $this->hangSanXuatDAO->Insert($hangSanXuat);
    }
    public function InsertName($tenLoaiSanPham)
    {
        $hangSanXuat = new LoaiSanPham();
        $hangSanXuat->TenSanLoaiPham= $tenLoaiSanPham;
    }
    public  function  SetDelete($maHangSanXuat)
    {
        $this->hangSanXuatDAO->SetDelete($maHangSanXuat);
    }
    public function UnsetDelete($maHangSanXuat)
    {
        $hangSanXuat = new LoaiSanPham();
        $hangSanXuat->MaHangSanXuat = $maHangSanXuat;
        $this->hangSanXuatDAO->UnsetDelete($hangSanXuat);
    }
    public function Update($maHangSanXuat)
    {
        $hangSanXuat = new LoaiSanPham();
        $hangSanXuat->MaHangSanXuat = $maHangSanXuat;
        $this->hangSanXuatDAO->Update($hangSanXuat);
    }

    //update tên hãng sx
    public function UpdateTen($ma, $ten)
    {
        $this->hangSanXuatDAO->UpdateTen($ma, $ten);
    }

    //update logo
    public function UpdateLogo($ma, $ten)
    {
        $this->hangSanXuatDAO->UpdateLogo($ma, $ten);
    }
}