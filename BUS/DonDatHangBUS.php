<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:55 AM
 */

class DonDatHangBUS
{
    var $donDatHangDAO;
    public function __construct()
    {
        $this->donDatHangDAO = new DonDatHangDAO();
    }
    public function GetAll()
    {
        return $this->donDatHangDAO->GetALL();
    }

    public function GetALLMATK($mataikhoan )
    {
        return $this->donDatHangDAO->GetALLMATK($mataikhoan);
    }


    public function InSert($donHang)
    {
        return $this->donDatHangDAO->Insert($donHang);
    }
    public function Delete($maDonHang)
    {
        return $this->donDatHangDAO->Delete($maDonHang);
    }
    public function Count()
    {
        return $this->donDatHangDAO->Count();
    }
    public function ktDonDatHang($ngaymua)
    {
        return $this->donDatHangDAO->ktDonDatHang($ngaymua);
    }
    public function Insertthamso($a,$b,$c,$d,$e)
    {
        $donHang = new DonDatHang();

        $donHang->MaDonDatHang= $a;
        $donHang->NgayLap=$b;
        $donHang->TongThanhTien=$c;
        $donHang->MaTaiKhoan=$d;
        $donHang->MaTinhTrang=$e;
        $this->donDatHangDAO->Insert($donHang);
    }
    public function Update($donhang)
    {
        return $this->donDatHangDAO->Update($donhang);
    }

    public function InsertTheoTaiKhoan($maTaiKhoan , $Tien)
    {
       $this->donDatHangDAO->InsertTheoTaiKhoan($maTaiKhoan, $Tien, $this->Count() + 1);

    }

    public function UpdateHUY($madon)
    {
        $this->donDatHangDAO->UpdateHUY($madon);
    }

    //update tien
    public function UpdateTien($id, $tien)
    {
        $this->donDatHangDAO->UpdateTien($id, $tien);
    }
}