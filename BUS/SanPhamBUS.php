<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:57 AM
 */

class SanPhamBUS
{
    var $sanPhamDAO;
    public function __construct()
    {
        $this->sanPhamDAO= new SanPhamDAO();
    }
    public function GetAll()
    {
        return $this->sanPhamDAO->GetAll();
    }
    public function Count()
    {
        return $this->sanPhamDAO->Count();
    }
    public function GetALLAvailable()
    {
        return $this->sanPhamDAO->GetAllAvailable();
    }
    public function GetByID($maSanPham)
    {
        return $this->sanPhamDAO->GetByID($maSanPham);
    }
    public function GetAllAvailableLoai($maLoaiSanPham)
    {
        return $this->sanPhamDAO->GetAllAvailableLoai($maLoaiSanPham);
    }
    public function GetAllAvailableLoai5($maLoaiSanPham)
    {
        return $this->sanPhamDAO->GetAllAvailableLoai5($maLoaiSanPham);
    }
    public function GetAllAvailableHang($maHangSanXuat)
    {
        return $this->sanPhamDAO->GetAllAvailableHang($maHangSanXuat);
    }
    public function GetAllAvailableHang_Loai($maHangSanXuat,$maLoaiSanPham)
    {
        if ($maHangSanXuat == null)
        {
            return $this->sanPhamDAO->GetAllAvailableLoai($maLoaiSanPham);
        }
        elseif ($maLoaiSanPham==null)
        {
            return $this->sanPhamDAO->GetAllAvailableHang($maHangSanXuat);
        }
        else
            return $this->sanPhamDAO->GetAllAvailableHang_Loai($maHangSanXuat,$maLoaiSanPham);
    }
    public function Insert($sanPham)
    {
        return $this->sanPhamDAO->Insert($sanPham);
    }
    public function InsertName($tenSanPham)
    {
        $sanPham = new SanPham();
        $sanPham->TenSanPham= $tenSanPham;
    }
//    public  function  Delete($maSanPham)
//    {
//        $loaiSanPham = new LoaiSanPham();
//        $loaiSanPham -> MaLoaiSanPham= $maSanPham;
//        $sosanphamloai = $this->sanPhamDAO->CountprojectofLoai($maSanPham);
//        if ($sosanphamloai == 0)
//            $this->sanPhamDAO->Delete($loaiSanPham);
//        else
//            $this->sanPhamDAO->SetDelete($loaiSanPham);
//
//    }
    public  function  SetDelete($maSanPham)
    {
        $this->sanPhamDAO->SetDelete($maSanPham);
    }
    public function UnsetDelete($maSanPham)
    {
        $sanPham = new LoaiSanPham();
        $sanPham->MaLoaiSanPham=$maSanPham;
        $this->sanPhamDAO->UnSetDelete($sanPham);
    }
    public function Update($maSanPham)
    {
        $sanPham = new LoaiSanPham();
        $sanPham->MaLoaiSanPham = $maSanPham;
        $this->sanPhamDAO->Update($sanPham);
    }
    public function ChiTietSP($maSanPham)
    {
        return $this->sanPhamDAO->ChiTietSP($maSanPham);
    }
    public function UpdateLuocXem($maSanPham)
    {
        return $this->sanPhamDAO->UpdateLuocXem($maSanPham);
    }
    public function UpdateTon($maSanPham,$sl)
    {
        return $this->sanPhamDAO->UpdateTon($maSanPham,$sl);
    }
    public function UpdateBan($maSanPham,$sl)
    {
        return $this->sanPhamDAO->UpdateBan($maSanPham,$sl);
    }
    public function TopSanPhamNew()
    {
        return $this->sanPhamDAO->TopSanPhamNew();
    }
    public function TopBanHang()
    {
        return $this->sanPhamDAO->TopBanHang();
    }
    //tìm kiếm đơn giản
    public function timkiemDG($timKiem)
    {
        return $this->sanPhamDAO->timkiemDG($timKiem);
    }

    //tìm kiếm theo nhà sản xuất
    public function timkiemNSX($timKiem)
    {
        return $this->sanPhamDAO->timkiemNSX($timKiem);
    }

    //tìm kiếm theo loại
    public function timkiemLoaSP($timKiem)
    {
        return $this->sanPhamDAO->timkiemLoaiSP($timKiem);
    }
    //tìm kiếm theo giá
    public function timkiemGia($tu , $den)
    {
        return $this->sanPhamDAO->timkiemGia($tu , $den);
    }
    //tìm kiếm theo mô tả
    public function timkiemMoTa($timKiem)
    {
        return $this->sanPhamDAO->timkiemMoTa($timKiem);
    }
    //update gia
    public function UpdateGia($masanpham , $sl)
    {
       $this->sanPhamDAO->UpdateGia($masanpham, $sl);
    }
    //update tên
    public function UpdateTen($ma, $ten)
    {
        $this->sanPhamDAO->UpdateTen($ma, $ten);
    }
}