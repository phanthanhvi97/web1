<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:27 AM
 */

class ChiTietDonHangBUS
{
    var $chiTietDonHangDAO;
    public function __construct()
    {
        $this->chiTietDonHangDAO =new ChiTietDonHangDAO();
    }
    public function GetAll()
    {
        return $this->chiTietDonHangDAO->GetAll();
    }
    public  function InsertTieuChi($a,$b,$c,$d,$e)
    {
        $ctDonHang = new ChiTietDonHang();
        $ctDonHang->MaChiTietDonDatHang= $a;
        $ctDonHang->SoLuong=$b;
        $ctDonHang->GiaBan=$c;
        $ctDonHang->MaDonDatHang=$d;
        $ctDonHang->MaSanPham=$e;
        $this->chiTietDonHangDAO->Insert($ctDonHang);
    }
    public function Count()
    {
        return $this->chiTietDonHangDAO->Count();
    }
    public function Insert($ctDonHang)
    {
        $this->chiTietDonHangDAO->Insert($ctDonHang);
    }
    //update giảm
    public function UpadteGiam($mact)
    {
        $this->chiTietDonHangDAO->UpadteGiam($mact);
    }
//update thêm
    public function UpdateThem($mact)
    {
        $this->chiTietDonHangDAO->UpdateThem($mact);
    }

}