<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/14/2018
 * Time: 9:47 PM
 */

class LoaiSanPhamDAO extends  DB
{
    // lấy hết sản phầm
    // dùng cho admin
    public function GetAll()
    {
        $sql = "select MaLoaisanPham, TenLoaiSanPham, BiXoa from loaisanpham";
        $result = $this ->ExecuteQuery($sql);
        $listLoaiSanPham = array();
        while ($row = mysqli_fetch_array($result))
        {
            extract($row);
            $loaiSanPham = new LoaiSanPham ();
            $loaiSanPham ->MaLoaiSanPham = $MaLoaisanPham;
            $loaiSanPham ->TenLoaiSanPham = $TenLoaiSanPham;
            $loaiSanPham ->BiXoa = $BiXoa;
            $listLoaiSanPham[] = $loaiSanPham;
        }
        return $listLoaiSanPham;
    }

    // ngoai menu thì chỉ hiện cho coi bị xóa = 0
    public function GetAllAvailable()
    {
        $sql = "select MaLoaisanPham, TenLoaiSanPham, BiXoa from loaisanpham where BiXoa = 0";
        $result = $this ->ExecuteQuery($sql);
        $listLoaiSanPham = array();
        while ($row = mysqli_fetch_array($result))
        {
            extract($row);
            $loaiSanPham = new LoaiSanPham ();
            $loaiSanPham ->MaLoaiSanPham = $MaLoaiSanPham;
            $loaiSanPham ->TenLoaiSanPham = $TenLoaiSanPham;
            $loaiSanPham ->BiXoa = $BiXoa;
            $listLoaiSanPham[] = $loaiSanPham;
        }
        return $listLoaiSanPham;
    }

    // lấy theo mã
    public function GetByID($maLoaiSanPham)
    {
        $sql = "select MaLoaisanPham, TenLoaiSanPham, BiXoa from loaisanpham where BiXoa = 0 and MaLoaiSanPham = $maLoaiSanPham";
        $result = $this->ExecuteQuery($sql);
        if($result == null)
            return ;
        $loaiSanPham = new LoaiSanPham();
        extract($result);
        $loaiSanPham -> TenLoaiSanPham = $TenLoaiSanPham;
        $loaiSanPham ->MaLoaiSanPham = $MaLoaiSanPham;
        $loaiSanPham ->BiXoa = $BiXoa;

        return $result;
    }
    //insert
    public function Insert ($loaiSanPham)
    {
        $sql = "INSERT INTO LoaiSanPham(MaLoaiSanPham,TenLoaiSanPham, BiXoa) VALUES($loaiSanPham->MaLoaiSanPham,'$loaiSanPham->TenLoaiSanPham',$loaiSanPham->BiXoa)";
        $this ->ExecuteQuery($sql);
    }

    //delete theo sản phẩm
    public function Delete($loaiSanPham)
    {
        $sql = "DELETE FROM LoaiSanPham WHERE MaLoaiSanPham = $loaiSanPham->MaLoaiSanPham";
        $this ->ExecuteQuery($sql);
    }
    //đánh dấu xóa theo sản phẩm
    public function SetDelete($maloaiSanPham)
    {
        $sql = "UPDATE LoaiSanPham SET BiXoa = 1 WHERE MaLoaiSanPham = $maloaiSanPham";
        $this ->ExecuteQuery($sql);
    }

    //đánh dấu bỏ xóa theo sản phẩm
    public function UnSetDelete($loaiSanPham)
    {
        $sql = "UPDATE LoaiSanPham SET BiXoa = 0 WHERE MaLoaiSanPham = $loaiSanPham->MaLoaiSanPham";
        $this ->ExecuteQuery($sql);
    }

    //update

    public function Update($loaiSanPham)
    {
        $sql = "UPDATE LoaiSanPham SET TenLoaiSanPham = $loaiSanPham->TenLoaiSanPham, BiXoa =  $loaiSanPham->BiXoa WHERE MaLoaiSanPham = $loaiSanPham->MaLoaiSanPham";
        $this ->ExecuteQuery($sql);
    }
    //update tên
    public function UpdateTen($ma, $ten)
    {
        $sql = "UPDATE LoaiSanPham SET TenLoaiSanPham = '$ten' WHERE MaLoaiSanPham = $ma";
        $this ->ExecuteQuery($sql);
    }

    //đếm số lượng sản phẩm theo mã sản phẩm
    public function DemSoSanPhamThuocVeLoai($maLoaiSanPham)
    {
        $sql = "Select count(MaLoaiSanPham) from SanPham where MaLoaiSanPham = $maLoaiSanPham";
        $result =$this ->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);

        return $row[0];
    }
    //count
    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from loaisanpham";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
}
?>