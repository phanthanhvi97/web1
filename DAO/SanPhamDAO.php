<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:21 AM
 */

class SanPhamDAO extends  DB
{
    public function  GetAll()
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham ";
        $result= $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem     = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    public function GetAllAvailable()
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where BiXoa=0";
        $result= $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }

    //lấy theo mã ản phẩm
    public function GetByID($maSanPham)
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,h.BiXoa,MaLoaiSanPham,TenHangSanXuat from sanpham as sp,hangsanxuat as h where sp.MaSanPham=$maSanPham and sp.MaHangSanXuat=h.MaHangSanXuat";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $row = mysqli_fetch_array($result);
        $sanPham = new SanPham();
        $sanPham->MaSanPham     = $row["MaSanPham"];
        $sanPham->TenSanPham    = $row["TenSanPham"];
        $sanPham->HinhURL       = $row["HinhURL"];
        $sanPham->GiaSanPham    = $row["GiaSanPham"];
        $sanPham->NgayNhap      = $row["NgayNhap"];
        $sanPham->SoLuongTon    = $row["SoLuongTon"];
        $sanPham->SoLuongBan    = $row["SoLuongBan"];
        $sanPham->SoLuocXem    = $row["SoLuocXem"];
        $sanPham->MoTa          = $row["MoTa"];
        $sanPham->BiXoa         = $row["BiXoa"];
        $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
        $sanPham->TenHangSanXuat = $row["TenHangSanXuat"];
        return $sanPham;
    }
    // hiển thị đồ chơi theo mã loại sản phẩm được chọn
    public function GetAllAvailableLoai($maLoaiSanPham)
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where MaLoaiSanPham='$maLoaiSanPham' and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }

    public function GetAllAvailableLoai5($maLoaiSanPham)
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where MaLoaiSanPham='$maLoaiSanPham' and BiXoa=0 limit 5";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    //chọn theo mã hảng sản xuất
    public function GetAllAvailableHang($maHangSanXuat)
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where MaHangSanXuat='$maHangSanXuat' and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    public function GetAllAvailableHang_Loai($maHangSanygXuat,$maLoaiSanPham)
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where MaHangSanXuat='$maHangSanXuat' and MaLoaiSanPham='$maLoaiSanPham' and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    public  function  Insert($sanPham)
    {
        $sql= "INSERT into sanpham(TenLoaiSanPham,BiXoa) values ($sanPham->MaSanPham,$sanPham->TenSanPham,$sanPham->HinhURL,$sanPham->GiaSanPham,$sanPham->NgayNhap,$sanPham->SoLuongTon,$sanPham->SoLuongBan,$sanPham->SoLuocXem,$sanPham->MoTa,$sanPham->BiXoa,$sanPham->MaLoaiSanPham,$sanPham->MaHangSanXuat)";
        $this->ExecuteQuery($sql);
    }

    public  function  Delete($sanPham)
    {
        $sql =" DELETE From sanpham WHERE MaSanPham = $sanPham->MaSanPham";
        $this->ExecuteQuery($sql);
    }
    public function SetDelete($masanPham)
    {
        $sql ="UPDATE sanpham Set BiXoa=1 where MaSanPham = $masanPham";
        $this->ExecuteQuery($sql);
    }
    public function UnSetDelete($sanPham)
    {
        $sql ="UPDATE sanpham Set BiXoa=0 where MaSanPham = $sanPham->MaSanPham";
        $this->ExecuteQuery($sql);
    }
    public function Update($sanPham)
    {
        $sql ="UPDATE sanpham Set MaSanPham=$sanPham->MaSanPham,TenSanPham=$sanPham->TenSanPham,HinhURL=$sanPham->HinhURL,GiaSanPham=$sanPham->GiaSanPham,NgayNhap=$sanPham->NgayNhap,SoLuongTon=$sanPham->SoLuongTon,SoLuongBan=$sanPham->SoLuongBan,SoLuocXem=$sanPham->SoLuocXem,MoTa=$sanPham->MoTa,BiXoa=$sanPham->BiXoa,MaLoaiSanPham=$sanPham->MaLoaiSanPham,MaHagSanXuat=$sanPham->MaHangSanXuat where MaSanPham = $sanPham->MaSanPham";
        $this->ExecuteQuery($sql);
    }
    //update tên
    public function UpdateTen($ma, $ten)
    {
        $sql ="UPDATE sanpham Set TenSanPham='$ten'  where MaSanPham = $ma";
        $this->ExecuteQuery($sql);
    }
    //update sl sản phẩm lại
    public function UpdateGia($masanpham, $sl)
    {
        $sql ="UPDATE sanpham set SoLuongTon = SoLuongTon - $sl, SoLuongBan=SoLuongBan + $sl where MaSanPham = $masanpham";
        $this->ExecuteQuery($sql);
    }
    public function UpdateLuocXem($maSanPham)
    {
        $sql ="UPDATE sanpham set SoLuocXem = SoLuocXem +1 where MaSanPham=$maSanPham";
        $this->ExecuteQuery($sql);
    }
    public function UpdateTon($maSanPham,$sl)
    {
        $sql ="UPDATE sanpham set SoLuongTon = (SoLuongTon -$sl) where MaSanPham=$maSanPham";
        $this->ExecuteQuery($sql);
    }
    public function UpdateBan($maSanPham,$sl)
    {
        $sql ="UPDATE sanpham set SoLuongBan = (SoLuongBan +$sl) where MaSanPham=$maSanPham";
        $this->ExecuteQuery($sql);
    }
    public function ChiTietSP($maSanPham)
    {
        $sql = "SELECT sp.MaSanPham ,sp.TenSanPham , sp.HinhURL, sp.GiaSanPham, h.TenHangSanXuat, l.TenLoaiSanPham,sp.SoLuongTon,sp.SoLuongBan,sp.SoLuocXem,sp.MoTa FROM  sanpham sp, loaisanpham l, hangsanxuat h WHERE sp.Bixoa =0 and sp.MaLoaiSanPham = l.MaLoaiSanPham and sp.MaHangSanXuat =h.MaHangSanXuat and sp.MaSanPham = $maSanPham";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $row = mysqli_fetch_array($result);
        $sanPham = new ChiTiet();
        $sanPham->MaSanPham =$row["MaSanPham"];
        $sanPham->TenSanPham = $row["TenSanPham"];
        $sanPham->HinhURL =$row["HinhURL"];
        $sanPham->GiaSanPham=$row["GiaSanPham"];
        $sanPham->TenHangSanXuat=$row["TenHangSanXuat"];
        $sanPham->TenLoaiSanPham=$row["TenLoaiSanPham"];
        $sanPham->SoLuongBan=$row["SoLuongBan"];
        $sanPham->SoLuongTon=$row["SoLuongTon"];
        $sanPham->SoLuocXem=$row["SoLuocXem"];
        $sanPham->MoTa=$row["MoTa"];
        return $sanPham;
    }
    //10 sản phẩm mới nhất
    public function TopSanPhamNew()
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham order by NgayNhap desc limit 10 ";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        if($result != null){
            while ($row =mysqli_fetch_array($result))
            {
                $sanPham = new SanPham();
                $sanPham->MaSanPham     = $row["MaSanPham"];
                $sanPham->TenSanPham    = $row["TenSanPham"];
                $sanPham->HinhURL       = $row["HinhURL"];
                $sanPham->GiaSanPham    = $row["GiaSanPham"];
                $sanPham->NgayNhap      = $row["NgayNhap"];
                $sanPham->SoLuongTon    = $row["SoLuongTon"];
                $sanPham->SoLuongBan    = $row["SoLuongBan"];
                $sanPham->SoLuocXem    = $row["SoLuocXem"];
                $sanPham->MoTa          = $row["MoTa"];
                $sanPham->BiXoa         = $row["BiXoa"];
                $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
                $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
                $lstSanPham[]= $sanPham;
            }
            return $lstSanPham;
        }
        else
        {
            return;
        }

    }

    // 10 sản phẩm bán chạy nhất
    public function TopBanHang()
    {
        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham order by SoLuongBan desc limit 10 ";
        $result = $this->ExecuteQuery($sql);
        $lstSanPham = array();
        if ($result != null)
        {
            while ($row =mysqli_fetch_array($result))
            {
                $sanPham = new SanPham();
                $sanPham->MaSanPham     = $row["MaSanPham"];
                $sanPham->TenSanPham    = $row["TenSanPham"];
                $sanPham->HinhURL       = $row["HinhURL"];
                $sanPham->GiaSanPham    = $row["GiaSanPham"];
                $sanPham->NgayNhap      = $row["NgayNhap"];
                $sanPham->SoLuongTon    = $row["SoLuongTon"];
                $sanPham->SoLuongBan    = $row["SoLuongBan"];
                $sanPham->SoLuocXem    = $row["SoLuocXem"];
                $sanPham->MoTa          = $row["MoTa"];
                $sanPham->BiXoa         = $row["BiXoa"];
                $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
                $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
                $lstSanPham[]= $sanPham;
            }

        }
        return $lstSanPham;
    }

    //tìm kiếm đơn giản theo tên
    public function timkiemDG($timKiem)
    {
        $sql = "SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where (TenSanPham like '%$timKiem%') and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    // tìm kiếm theo nhà sản xuất
    public function timkiemNSX($timKiem)
    {
//        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where (TenSanPham like '%$timKiem%') and BiXoa=0";
        $sql = "SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,s.BiXoa,MaLoaiSanPham,s.MaHangSanXuat from sanpham s, hangsanxuat h where s.BiXoa=0 and s.MaHangSanXuat = h.MaHangSanXuat and h.TenHangSanXuat like '%$timKiem%' and h.BiXoa = 0";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    //tìm kiếm theo loại sản phầm
    public function timkiemLoaiSP($timKiem)
    {
//        $sql ="SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where (TenSanPham like '%$timKiem%') and BiXoa=0";
        $sql = "SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,s.BiXoa,s.MaLoaiSanPham,s.MaHangSanXuat from sanpham s, loaisanpham h where s.BiXoa=0 and s.MaLoaiSanPham = h.MaLoaiSanPham and h.TenLoaiSanPham like '%$timKiem%' and h.BiXoa = 0";$result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    //tìm kiếm theo giá

    public function timkiemGia($tu, $den)
    {
        $sql = "SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where (GiaSanPham >= $tu and GiaSanPham<=$den ) and BiXoa=0";
       $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
    //theo mô tả
    public function timkiemMoTa($timKiem)
    {
        $sql = "SELECT MaSanPham,TenSanPham,HinhURL,GiaSanPham,NgayNhap,SoLuongTon,SoLuongBan,SoLuocXem,MoTa,BiXoa,MaLoaiSanPham,MaHangSanXuat from sanpham where (MoTa like '%$timKiem%') and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $lstSanPham = array();
        while ($row =mysqli_fetch_array($result))
        {
            $sanPham = new SanPham();
            $sanPham->MaSanPham     = $row["MaSanPham"];
            $sanPham->TenSanPham    = $row["TenSanPham"];
            $sanPham->HinhURL       = $row["HinhURL"];
            $sanPham->GiaSanPham    = $row["GiaSanPham"];
            $sanPham->NgayNhap      = $row["NgayNhap"];
            $sanPham->SoLuongTon    = $row["SoLuongTon"];
            $sanPham->SoLuongBan    = $row["SoLuongBan"];
            $sanPham->SoLuocXem    = $row["SoLuocXem"];
            $sanPham->MoTa          = $row["MoTa"];
            $sanPham->BiXoa         = $row["BiXoa"];
            $sanPham->MaLoaiSanPham = $row["MaLoaiSanPham"];
            $sanPham->MaHangSanXuat = $row["MaHangSanXuat"];
            $lstSanPham[]= $sanPham;
        }
        return $lstSanPham;
    }
//count
    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from sanpham";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
}  