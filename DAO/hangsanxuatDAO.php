<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:21 AM
 */

class HangSanXuatDAO extends  DB
{
    public function  GetAll()
    {
        $sql ="SELECT MaHangSanXuat, TenHangSanXuat,LogoURL, BiXoa from hangsanxuat ";
        $result= $this->ExecuteQuery($sql);
        $lstHangSanXuat = array();
        while ($row =mysqli_fetch_array($result))
        {
            extract($row);
            $hangSanXuat = new HangSanXuat();
            $hangSanXuat->MaHangSanXuat     = $row["MaHangSanXuat"];
            $hangSanXuat->TenHangSanXuat    = $row["TenHangSanXuat"];
            $hangSanXuat->LogoURL           =$row["LogoURL"];
            $hangSanXuat->BiXoa             = $row["BiXoa"];
            $lstHangSanXuat[]= $hangSanXuat;
        }
        return $lstHangSanXuat;
    }
    public function GetAllAvailable()
    {
        $sql = "SELECT MaHangSanXuat, TenHangSanXuat,LogoURL,BiXoa from hangsanxuat where BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        $lstHangSanXuat =array();
        while ($row= mysqli_fetch_array($result))
        {
            $hangSanXuat = new HangSanXuat();
            $hangSanXuat->MaHangSanXuat= $row["MaHangSanXuat"];
            $hangSanXuat->TenHangSanXuat= $row["TenHangSanXuat"];
            $hangSanXuat->LogoURL=$row["LogoURL"];
            $hangSanXuat->BiXoa= $row["BiXoa"];
            $lstHangSanXuat[]= $hangSanXuat;
        }
        return $lstHangSanXuat;
    }
    public function GetByID($maHangSanXuat)
    {
        $sql = "SELECT MaHangSanXuat, TenHangSanXuat,BiXoa from hangsanxuat where MaHangSanXuat=$maHangSanXuat";
        $result = $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $row = mysqli_fetch_array($result);
        $hangSanXuat = new HangSanXuat();
        $hangSanXuat->MaHangSanXuat = $row["MaHangSanXuat"];
        $hangSanXuat->TenHangSanXuat = $row["TenHangSanXuat"];
        $hangSanXuat->BiXoa = $row["BiXoa"];
        return $hangSanXuat;
    }
    //insert
    public  function  Insert($hangSanXuat)
    {
        $sql= "INSERT into hangsanxuat(MaHangSanXuat,TenHangSanXuat,LogoURL,BiXoa) values ($hangSanXuat->MaHangSanXuat,'$hangSanXuat->TenHangSanXuat','$hangSanXuat->LogoURL',$hangSanXuat->BiXoa)";
        $this->ExecuteQuery($sql);
    }
    public  function  Delete($hangSanXuat)
    {
        $sql =" DELETE From hangsanxuat WHERE MaHangSanXuat = $hangSanXuat->MaHangSanXuat";
        $this->ExecuteQuery($sql);
    }
    public function SetDelete($mahangSanXuat)
    {
        $sql ="UPDATE hangsanxuat Set BiXoa=1 where MaHangSanXuat = $mahangSanXuat";
        $this->ExecuteQuery($sql);
    }
    public function UnSetDelete($hangSanXuat)
    {
        $sql ="UPDATE hangsanxuat Set BiXoa=0 where MaHangSanXuat = $hangSanXuat->MaHangSanXuat";
        $this->ExecuteQuery($sql);
    }
    public function Update($hangSanXuat)
    {
        $sql ="UPDATE hangsanxuat Set TenHangSanXuat = '$hangSanXuat->TenHangSanXuat', BiXoa=$hangSanXuat->BiXoa where MaHangSanXuat = $hangSanXuat->MaHangSanXuat";
        $this->ExecuteQuery($sql);
    }
//update tên hãng sx
    public function UpdateTen($ma, $ten)
    {
        $sql ="UPDATE hangsanxuat Set TenHangSanXuat = '$ten' where MaHangSanXuat =$ma";
        $this->ExecuteQuery($sql);
    }

    //update logo
    public function UpdateLogo($ma, $ten)
    {
        $sql ="UPDATE hangsanxuat Set LogoURL = '$ten' where MaHangSanXuat =$ma";
        $this->ExecuteQuery($sql);
    }
    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from hangsanxuat";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
}