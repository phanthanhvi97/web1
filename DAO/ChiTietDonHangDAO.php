<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:19 AM
 */

class ChiTietDonHangDAO extends DB
{
    public function GetAll()
    {
        $sql ="SELECT MaChiTietDonDatHang,SoLuong,GiaBan,MaDonDatHang,MaSanPham from chitietdondathang";
        $result= $this->ExecuteQuery($sql);
        $lstCTDonHang = array();
        while ($row =mysqli_fetch_array($result))
        {
            $ctDonHang = new ChiTietDonHang();
            $ctDonHang->MaChiTietDonDatHang   = $row["MaChiTietDon"];
            $ctDonHang->SoLuong               = $row["SoLuong"];
            $ctDonHang->GiaBan                = $row["GiaBan"];
            $ctDonHang->MaDonDatHang          = $row["MaDonDatHang"];
            $ctDonHang->MaSanPham             = $row["MaSanPham"];
            $lstCTDonHang[] = $ctDonHang;
        }
        return $lstCTDonHang;
    }
    public function Insert($ctDonHang)
    {
        $sql= "INSERT into chitietdondathang(MaChiTietDonDatHang,SoLuong,GiaBan,MaDonDatHang,MaSanPham) values ('$ctDonHang->MaChiTietDonDatHang',$ctDonHang->SoLuong,$ctDonHang->GiaBan,'$ctDonHang->MaDonDatHang','$ctDonHang->MaSanPham')";
        $this->ExecuteQuery($sql);
    }
    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from chitietdondathang";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
    //giá của một sản phẩm vì phục vụ cho nhu cầu thay đổi số tiền
    public function Gia($mact)
    {
        $sql = "select GiaBan from chitietdondathang where  MaChiTietDonDatHang = '$mact'";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["GiaBan"];
    }
    // update thêm 1
    public function UpdateThem($mact)
    {
        $sql = " UPDATE chitietdondathang set SoLuong = SoLuong + 1 where MaChiTietDonDatHang = '$mact'";
        $this->ExecuteQuery($sql);
        $gia = $this->Gia($mact);
        $sql = " UPDATE dondathang set TongThanhTien = (TongThanhTien + $gia)  where MaDonDatHang = (select MaDonDatHang from chitietdondathang where  MaChiTietDonDatHang = '$mact' )";
        $this->ExecuteQuery($sql);
    }


//update giảm 1
    public function UpadteGiam($mact)
    {
        $sql = " UPDATE chitietdondathang set SoLuong = SoLuong - 1 where MaChiTietDonDatHang = '$mact'";
        $this->ExecuteQuery($sql);
        $gia = $this->Gia($mact);
        $sql = " UPDATE dondathang set TongThanhTien = TongThanhTien - $gia  where MaDonDatHang = (select MaDonDatHang from chitietdondathang where  MaChiTietDonDatHang = '$mact' )";
        $this->ExecuteQuery($sql);
    }

}