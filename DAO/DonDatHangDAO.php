<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:20 AM
 */

class DonDatHangDAO extends  DB
{
    public function GetALL()
    {
        $sql ="SELECT MaDonDatHang,NgayLap,TongThanhTien,MaTaiKhoan,MaTinhTrang from dondathang";
        $result= $this->ExecuteQuery($sql);
        $lstDonHang = array();
        while ($row =mysqli_fetch_array($result)) {
            $donHang = new DonDatHang();
            $donHang->MaDonDatHang = $row["MaDonDatHang"];
            $donHang->NgayLap = $row["NgayLap"];
            $donHang->TongThanhTien = $row["TongThanhTien"];
            $donHang->MaTaiKhoan = $row["MaTaiKhoan"];
            $donHang->MaTinhTrang = $row["MaTinhTrang"];
            $lstDonHang[] = $donHang;
        }
        return $lstDonHang;
    }

    //lấy sản phẩm theo tài khoản
    public function GetALLMATK($mataikhoan )
    {
       // $sql = "select d.MaDonDatHang, NgayLap, TongThanhTien, MaTaiKhoan, MaTinhTrang, c.MaChiTietDonDatHang, c.SoLuong, c.GiaBan, c.MaSanPham from dondathang d, chitietdondathang c WHERE d.MaDonDatHang = c.MaDonDatHang and MaTaiKhoan = $mataikhoan ";
        $sql = "select t.MaTinhTrang, d.MaDonDatHang,TongThanhTien,t.TenTinhTrang, c.MaChiTietDonDatHang, c.SoLuong, c.GiaBan, c.MaSanPham , s.HinhURL, s.TenSanPham, s.MoTa from tinhtrang t,  dondathang d, chitietdondathang c, sanpham s WHERE d.MaDonDatHang = c.MaDonDatHang and s.MaSanPham = c.MaSanPham and t.MaTinhTrang = d.MaTinhTrang and MaTaiKhoan = $mataikhoan and t.MaTinhTrang <> 4 ";

        $result= $this->ExecuteQuery($sql);
        $lstDonHang = array();
        while ($row =mysqli_fetch_array($result)) {
            $donHang = new DonDatHang();
            $donHang->MaDonDatHang = $row["MaDonDatHang"];
            $donHang->TongThanhTien = $row["TongThanhTien"];
            $donHang->TenTinhTrang = $row["TenTinhTrang"];
            $donHang->MaChiTietDonDatHang = $row["MaChiTietDonDatHang"];
            $donHang->SoLuong = $row["SoLuong"];
            $donHang->MaTinhTrang = $row["MaTinhTrang"];

            $donHang->GiaBan = $row["GiaBan"];
            $donHang->MaSanPham = $row["MaSanPham"];
            $donHang->HinhURL = $row["HinhURL"];
            $donHang->TenSanPham = $row["TenSanPham"];
            $donHang->MoTa = $row["MoTa"];
            $lstDonHang[] = $donHang;
        }
        return $lstDonHang;
    }
    public function ktDonDatHang($ngaymua)
    {
        $sql ="SELECT MaDonDatHang,NgayLap,TongThanhTien,MaTaiKhoan,MaTinhTrang from dondathang where NgayLap like '$ngaymua%' order by MaDonDatHang desc limit 1 ";
        $result= $this->ExecuteQuery($sql);
        if ($result == null)
            return null;
        $row = mysqli_fetch_array($result);
        $donHang = new DonDatHang();
        $donHang->MaDonDatHang = $row["MaDonDatHang"];
        $donHang->NgayLap = $row["NgayLap"];
        $donHang->TongThanhTien = $row["TongThanhTien"];
        $donHang->MaTaiKhoan = $row["MaTaiKhoan"];
        $donHang->MaTinhTrang = $row["MaTinhTrang"];
        return $donHang;
    }
    public function Update($donHang)
    {
        $sql = "UPDATE dondathang SET NgayLap =  N'$donHang->NgayLap' TongThanhTien=$donHang->TongThanhTien,MaTaiKhoan=$donHang->MaTaiKhoan,MaTinhTrang=$donHang->MaTinhTrang where MaDonDatHang = N'$donHang->MaDonDatHang'";
        $this->ExecuteQuery($sql);
    }
    //update tien
    public function UpdateTien($id, $tien)
    {
        $sql = "UPDATE dondathang SET TongThanhTien=$tien where MaDonDatHang = N'$id'";
        $this->ExecuteQuery($sql);
    }
    public function Insert($donHang)
    {
        $sql= "INSERT into dondathang(MaDonDatHang,TongThanhTien,MaTaiKhoan,MaTinhTrang) values ('$donHang->MaDonDatHang',$donHang->TongThanhTien,$donHang->MaTaiKhoan,$donHang->MaTinhTrang)";
       $a= $this->ExecuteQuery($sql);

    }
    public function Delete($madonHang)
    {
        $sql= "Delete dondathang WHERE MaDonDatHang=$madonHang";
        $this->ExecuteQuery($sql);
    }
    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from dondathang";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
//khi người dùng chọn mua thì lập tức nó ad vào đơn hàng
    public function InsertTheoTaiKhoan($maTaiKhoan , $Tien, $ma)
    {

        $donHang = new DonDatHang();
        $donHang->MaDonDatHang =$ma;
        $donHang->MaTaiKhoan = $maTaiKhoan;
        $donHang->MaTinhTrang = 1;
        $donHang->TongThanhTien= $Tien;
        $this->Insert($donHang);

    }
    //update tình trạng là đã hủy
    public function UpdateHUY($madon)
    {
        $sql = " UPDATE dondathang set MaTinhTrang = 4 where MaDonDatHang = '$madon'";
        $this->ExecuteQuery($sql);
    }

}