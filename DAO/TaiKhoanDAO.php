<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/15/2018
 * Time: 1:22 AM
 */


class TaiKhoanDAO extends DB
{
    // xem tất cả
    public function GetAll()
    {
        $sql = "SELECT MaTaiKhoan,TenDangNhap,MatKhau,TenHienThi,DiaChi,DienThoai,Email,BiXoa,MaLoaiTaiKhoan From taikhoan";
        $result = $this->ExecuteQuery($sql);
        $lstTaiKhoan = array();
        while ($row =mysqli_fetch_array($result))
        {
            $taiKhoan = new TaiKhoan();
            $taiKhoan->MaTaiKHoan     =$row["MaTaiKhoan"];
            $taiKhoan->TenDangNhap    =$row["TenDangNhap"];
            $taiKhoan->MatKhau        =$row["MatKhau"];
            $taiKhoan->TenHienThi     =$row["TenHienThi"];
            $taiKhoan->DiaChi         =$row["DiaChi"];
            $taiKhoan->DienThoai      =$row["DienThoai"];
            $taiKhoan->Email          =$row["Email"];
            $taiKhoan->BiXoa          =$row["BiXoa"];
            $taiKhoan->MaLoaiTaiKhoan =$row["MaLoaiTaiKhoan"];
            $lstTaiKhoan[]=$taiKhoan;
        }
        return $lstTaiKhoan;
    }
    // người dùng xem
    public function GetAllAvailable()
    {
        $sql = "SELECT MaTaiKhoan,TenDangNhap,MatKhau,TenHienThi,DiaChi,DienThoai,Email,BiXoa,MaLoaiTaiKhoan from taikhoan where BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        $lstTaiKhoan = array();
        while ($row =mysqli_fetch_array($result))
        {
            $taiKhoan = new TaiKhoan();
            $taiKhoan->MaTaiKhoan     =$row["MaTaiKhoan"];
            $taiKhoan->TenDangNhap    =$row["TenDangNhap"];
            $taiKhoan->MatKhau        =$row["MatKhau"];
            $taiKhoan->TenHienThi     =$row["TenHienThi"];
            $taiKhoan->DiaChi         =$row["DiaChi"];
            $taiKhoan->DienThoai      =$row["DienThoai"];
            $taiKhoan->Email          =$row["Email"];
            $taiKhoan->BiXoa          =$row["BiXoa"];
            $taiKhoan->MaLoaiTaiKhoan =$row["MaLoaiTaiKhoan"];
            $lstTaiKhoan[]=$taiKhoan;
        }
        return $lstTaiKhoan;
    }


    public function GetID($id)
    {
        $sql = "SELECT MaTaiKhoan,TenDangNhap,MatKhau,TenHienThi,DiaChi,DienThoai,Email,BiXoa,MaLoaiTaiKhoan from taikhoan where MaTaiKhoan = $id and BiXoa=0";
        $result = $this->ExecuteQuery($sql);
        while ($row =mysqli_fetch_array($result))
        {
            $taiKhoan = new TaiKhoan();
            $taiKhoan->MaTaiKhoan     =$row["MaTaiKhoan"];
            $taiKhoan->TenDangNhap    =$row["TenDangNhap"];
            $taiKhoan->MatKhau        =$row["MatKhau"];
            $taiKhoan->TenHienThi     =$row["TenHienThi"];
            $taiKhoan->DiaChi         =$row["DiaChi"];
            $taiKhoan->DienThoai      =$row["DienThoai"];
            $taiKhoan->Email          =$row["Email"];
            $taiKhoan->BiXoa          =$row["BiXoa"];
            $taiKhoan->MaLoaiTaiKhoan =$row["MaLoaiTaiKhoan"];
            return $taiKhoan;
        }
       return;
    }


    // đăng nhập nek
    public function xllogin($us,$ps)
    {
        $sql = "Select MaTaiKhoan,MaLoaiTaiKhoan,TenHienThi,TenDangNhap,MatKhau,BiXoa from taikhoan where BiXoa=0 and TenDangNhap='$us' and MatKhau='$ps' ";
        $result = $this->ExecuteQuery($sql);
        $row =mysqli_fetch_array($result);
        if ($row==null)
        {
            return null;
        }
        else
        {
            $taiKhoan = new TaiKhoan();
            $taiKhoan->MaTaiKhoan     =$row["MaTaiKhoan"];
            $taiKhoan->TenDangNhap    =$row["TenDangNhap"];
            $taiKhoan->MatKhau        =$row["MatKhau"];
            $taiKhoan->TenHienThi     =$row["TenHienThi"];
//        $taiKhoan->DiaChi         =$row["DiaChi"];
//        $taiKhoan->DienThoai      =$row["DienThoai"];
//        $taiKhoan->Email          =$row["Email"];
            $taiKhoan->BiXoa          =$row["BiXoa"];
            $taiKhoan->MaLoaiTaiKhoan =$row["MaLoaiTaiKhoan"];
            return $taiKhoan;
        }
    }


    public function ktnamelogin($name)
    {
        $sql="Select TenDangNhap from taikhoan where TenDangNhap = '$name'";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return 1;
    }

    public  function CreateAccountCode()
    {
        $sql ="Select MaTaiKhoan from taikhoan Order by MaTaiKhoan desc limit 1";
        $result = $this->ExecuteQuery($sql);

        if ($result == null)
        {
            return 0;
        }
        $row = mysqli_fetch_array($result);
        return $row["MaTaiKhoan"];
    }
    // thêm tài khoản
    public  function Insert($taiKhoan)
    {

        $sql = "Insert into taikhoan(MaTaiKhoan,TenDangNhap,MatKhau,TenHienThi,DiaChi,DienThoai,Email,BiXoa,MaLoaiTaiKhoan) values ($taiKhoan->MaTaiKHoan,'$taiKhoan->TenDangNhap','$taiKhoan->MatKhau','$taiKhoan->TenHienThi','$taiKhoan->DiaChi','$taiKhoan->DienThoai','$taiKhoan->Email',$taiKhoan->BiXoa,$taiKhoan->MaLoaiTaiKhoan)";
        $this->ExecuteQuery($sql);

    }
    //xóa tài khoản
    public  function  Delete($taiKhoan)
    {
        $sql =" DELETE From taikhoan WHERE MaTaiKhoan = '$taiKhoan'";
        $this->ExecuteQuery($sql);
    }
    //đánh dấu xóa
    public function SetDelete($taiKhoan)
    {
        $sql ="UPDATE taikhoan Set BiXoa = 1 where MaTaiKhoan =$taiKhoan";
        $this->ExecuteQuery($sql);
    }
    ///gỡ xóa
    public function UnSetDelete($taiKhoan)
    {
        $sql ="UPDATE taikhoan Set BiXoa=0 where MaTaiKhoan = $taiKhoan->MaTaiKhoan";
        $this->ExecuteQuery($sql);
    }

    //
    public function ktHDTaiKhoan($taiKhoan)
    {
        $sql ="Select MaTaiKhoan from taikhoan where MaTaiKhoan = '$taiKhoan' and (MaTaiKhoan in (SELECT MaTaiKhoan from dondathang)) ";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return 1;
    }
    public function SetCategory($taiKhoan)
    {
        $sql ="UPDATE taikhoan Set MaLoaiTaiKhoan=3-MaLoaiTaiKhoan where MaTaiKhoan ='$taiKhoan'";
        $this->ExecuteQuery($sql);
    }
    public function Update($taiKhoan)
    {
        $sql ="UPDATE loaisanpham Set MaTaiKhoan=$taiKhoan->MaTaiKhoan,TenDangNhap=$taiKhoan->TenDangNhap,MatKhau=$taiKhoan->MatKhau,TenHienThi= $taiKhoan->TenHienThi,NgaySinh=$taiKhoan->NgaySinh,DiaChi=$taiKhoan->DiaChi,DienThoai=$taiKhoan->DienThoai,Email=$taiKhoan->Email,BiXoa=$taiKhoan->BiXoa,MaLoaiTaiKhoan=$taiKhoan->MaLoaiTaiKhoan where MaTaiKhoan = $taiKhoan->MaTaiKhoan";
        $this->ExecuteQuery($sql);
    }

    public function Count()
    {
        $sql = "SELECT COUNT(*) sl from taikhoan";
        $result = $this->ExecuteQuery($sql);
        $row = mysqli_fetch_array($result);
        if ($row == null)
        {
            return 0;
        }
        return $row["sl"];
    }
//update theo tên đăng nhập
    public function UpdateTenDN($id, $tendangnhap)
    {
        $sql="UPDATE taikhoan SET TenDangNhap = '$tendangnhap' where MaTaiKhoan = $id";
        $this->ExecuteQuery($sql);
    }

    //update theo matkhau
    public function UpdateMK($id, $matkhau)
    {
        $sql="UPDATE taikhoan SET MatKhau = '$matkhau' where MaTaiKhoan = $id";
        $this->ExecuteQuery($sql);
    }

    //update theo tên hiển thị
    public function UpdateTenHienThi($id, $tenhienthi)
    {
        $sql="UPDATE taikhoan SET TenHienThi = '$tenhienthi' where MaTaiKhoan = $id";
        $this->ExecuteQuery($sql);
        echo"$id";
    }
}