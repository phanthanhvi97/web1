<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/14/2018
 * Time: 8:35 PM
 */

class TaiKhoan
{
    var $MaTaiKHoan;
    var $TenDangNhap;
    var $MatKhau;
    var $TenHienThi;
    var $DiaChi;
    var $DienThoai;
    var $Email;
    var $BiXoa;
    var $MaLoaiTaiKhoan;
    var $NgaySinh;

    public function __construct()
    {
        $this -> MaLoaiTaiKhoan = 1;
        $this ->TenDangNhap ="";
        $this ->MatKhau ="";
        $this -> TenHienThi ="";
        $this -> DiaChi ="";
        $this ->DienThoai ="";
        $this ->Email ="";
        $this ->BiXoa = 0;
        $this->MaTaiKHoan = 0;
        $this->NgaySinh ="";
    }
}
?>