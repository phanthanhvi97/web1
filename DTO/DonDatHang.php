<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/14/2018
 * Time: 8:24 PM
 */

class DonDatHang
{
    var $MaDonHang ;
    var $NgayLap;
    var $TongThanhTien;
    var $MaTaiKHoan;
    var $MaTinhTrang;

    public function __construct()
    {
        $this ->MaDonHang ="";
        $this -> NgayLap = "";
        $this ->TongThanhTien = 0;
        $this -> MaTinhTrang = 0;
        $this ->MaTaiKHoan = 0;
    }
}
?>