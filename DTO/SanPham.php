<?php
/**
 * Created by PhpStorm.
 * User: Thuy Ngo
 * Date: 12/14/2018
 * Time: 8:30 PM
 */

class SanPham
{
    var $MaSanPham;
    var $TenSanPham;
    var $HinhURL;
    var $GiaSanPham;
    var $NgayNhap;
    var $SoLuongTon;
    var $SoLuongBan;
    var $SoLuocXem;
    var $MoTa;
    var $BiXoa;
    var $MaLoaiSanPham;
    var $MaHangSanXuat;
    public function __construct()
    {
        $this->MaSanPham=0;
        $this->TenSanPham="";
        $this->HinhURL="";
        $this->GiaSanPham=0;
        $this->NgayNhap="";
        $this->SoLuongTon=0;
        $this->SoLuongBan=0;
        $this->SoLuocXem=0;
        $this->MoTa="";
        $this->BiXoa=0;
        $this->MaLoaiSanPham=0;
        $this->MaHangSanXuat=0;
    }
}
?>